import {LoginPage} from "../pages/LoginPage";

describe('MY ACCOUNT - LOGIN', () => {
    let loginPage
    let myAccountPage

    beforeEach('Open Login Page', () => {
        cy.openLoginPage()
        loginPage = new LoginPage()
    })

    it('Log-in with valid username and password.', () => {

        myAccountPage = loginPage.loginSuccessful(Cypress.env('email_address'), Cypress.env('login_password'))

        // User must successfully login to the web page
        myAccountPage.assertIsLoggedIn(Cypress.env('username'))
    })

    it('Log-in with incorrect username and incorrect password.', () => {

        loginPage.login(Cypress.env('incorrect_email'), Cypress.env('login_password'))

        // Proper error must be displayed(ie Invalid username) and prompt to enter login again
        loginPage.validateErrorMessage('Error: A user could not be found with this email address.')
    })
})

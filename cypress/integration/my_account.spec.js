import {LoginPage} from "../pages/LoginPage";

describe('MY ACCOUNT', () => {
    let loginPage
    let myAccountPage
    let navigation

    beforeEach('Open Login Page', () => {
        cy.openLoginPage()
        loginPage = new LoginPage()
        // Login
        myAccountPage = loginPage.loginSuccessful(Cypress.env('email_address'), Cypress.env('login_password'))
        navigation = myAccountPage.navigation
    })

    it('My Accounts-Orders', () => {
        // Click on Orders link
        navigation.openOrders()

    })

    it('My Accounts-Downloads', () => {
        // Click on Orders link
        navigation.openDownloads()

    })

    it('My Accounts-Log-Out', () => {
        // Click on Logout button
        navigation.clickLogoutButton()

        // On clicking logout,User successfully comes out from the account
        loginPage.assertIsLoggedOut()
    })
})

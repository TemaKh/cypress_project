import {ShopPage} from "../pages/ShopPage";

describe('SHOP', () => {
    let shopPage

    beforeEach('Open Shop Page', () => {
        cy.openShopPage()
        shopPage = new ShopPage()
    })

    it('Sort by Popularity item', () => {
        // Click on Sort by Popularity item in Default sorting dropdown
        shopPage.selectSortBy('Sort by popularity')
        // Now user can view the popular products only
    })

})
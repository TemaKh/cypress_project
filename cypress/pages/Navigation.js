import {OrdersPage} from "./OrdersPage";
import {DownloadsPage} from "./DownloadsPage";

export  class Navigation {
    constructor() {
        this.navigation.should('be.visible')
    }

    get navigation() {
        return cy.get('.woocommerce-MyAccount-navigation')
    }

    get ordersLink() {
        return cy.get('.woocommerce-MyAccount-navigation-link--orders').contains('Orders')
    }

    get downloadsLink() {
        return cy.get('.woocommerce-MyAccount-navigation-link--downloads').contains('Downloads')
    }

    get logoutButton() {
        return cy.get('.woocommerce-MyAccount-navigation-link--customer-logout').contains('Logout')
    }

    openOrders() {
        this.ordersLink.click()
        return new OrdersPage()
    }

    openDownloads() {
        this.downloadsLink.click()
        return new DownloadsPage()
    }

    clickLogoutButton() {
        this.logoutButton.click()
    }
}
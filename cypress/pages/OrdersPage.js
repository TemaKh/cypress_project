export class OrdersPage {
    constructor() {
        cy.url().should('include', Cypress.env('login_url') + '/orders')
    }
}
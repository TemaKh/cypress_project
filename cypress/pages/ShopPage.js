export class ShopPage {
    constructor() {

    }

    get sortingDropdown() {
        return cy.get('.orderby')
    }

    selectSortBy(name) {
        this.sortingDropdown.select(name)
    }
}
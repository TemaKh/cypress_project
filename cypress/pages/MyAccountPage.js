import {Navigation} from "./Navigation";

export  class MyAccountPage {
    navigation;

    constructor() {
        this.navigation = new Navigation()
    }

    get contentsPage() {
        return cy.get('div[class="woocommerce"]')
    }

    get downloadsLink() {
        return cy.get('ul').contains('Downloads')
    }

    assertIsLoggedIn(username) {
        this.contentsPage.should('contain.text', 'Hello ' + username)
    }
}

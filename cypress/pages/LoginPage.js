import {MyAccountPage} from "./MyAccountPage";

export  class LoginPage {
    constructor() {
        this.usernameInput.should('be.visible')
        this.passwordInput.should('be.visible')
        this.loginButton.should('be.visible')
    }

    get usernameInput() {
        return cy.get('#username')
    }

    get passwordInput() {
        return cy.get('#password')
    }

    get loginButton() {
        return cy.get('input[name="login"]')
    }

    get errorMessage() {
        return cy.get('ul[class="woocommerce-error"]')
    }

    login(username, password) {
        this.enterUsername(username)
        this.enterPassword(password)
        this.clickLoginButton()
    }

    loginSuccessful(username, password) {
        this.login(username, password)
        return new MyAccountPage()
    }

    validateErrorMessage(message) {
        this.errorMessage.should('contain.text', message)
    }

    assertIsLoggedOut() {
        cy.url().should('include', Cypress.env('login_url'))
        this.usernameInput.should('be.visible')
        this.passwordInput.should('be.visible')
    }

    enterUsername(username) {
        this.usernameInput.type(username)
    }

    enterPassword(password) {
        this.passwordInput.type(password)
    }

    clickLoginButton() {
        this.loginButton.click()
    }
}

export class DownloadsPage {
    constructor() {
        cy.url().should('include', Cypress.env('login_url') + '/downloads')
    }
}